@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-4">
            <x-card title="Create Product Category">
                <form action="{{route('product-category.store')}}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name">Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control required" id="name" name="name" placeholder="Product Category Name" required/>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="comment">Comment</label>
                                <input type="text" class="form-control" id="comment" name="comment" placeholder="Comment"/>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="name">State <span class="text-danger">*</span></label>
                            <select class="select-2 form-control" name="state" required>
                                @foreach($viewModel->state as $stateKey => $stateValue)
                                    <option value="{{$stateKey}}">{{$stateValue}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary mr-2 float-right">Submit</button>
                    </div>

                </form>
            </x-card>
        </div>

        <div class="col-md-8">

            <x-card title="All Product Category" toolbar="yes">
                <table class="table table-separate table-head-custom table-checkable" id="non_hsn_table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>State</th>
                            <th>Comment</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Drawing Books</td>
                            <td>Active</td>
                            <td>This category belongs to Drawing Book</td>
                            <td><a href="{{route('product-category.edit', [1])}}" class="edit-non-hsn-btn"><i class="fas fa-pencil-alt"></i></a></td>
                        </tr>
                    </tbody>
                </table>
            </x-card>


        </div>
    </div>
@endsection

@section('styles')
    <!--begin::Page Vendors Styles(used by this page)-->
    <link href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
    <!--end::Page Vendors Styles-->

    <style>
        .edit-non-hsn-btn:hover i{
            color: #3699FF;
        }
    </style>
@endsection

@section('scripts')
    <!--begin::Page Vendors(used by this page)-->
    <script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>
    <!--end::Page Vendors-->
    <script>
        var arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        };
    </script>
@endsection
