@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <x-card title="Hello World">
                    <p>Card body</p>
                    <x-slot name="footer">
                        <button type="submit" class="btn btn-primary mr-2"  data-toggle="modal" data-target="#confirmModal">Submit</button>
                        <button type="reset" class="btn btn-secondary">Cancel</button>
                    </x-slot>
                </x-card>
            </div>
            <div class="col-md-12">
                <x-card title="Form Card">
                    <x-form action="http://www.google.com">
                        FORM
                    </x-form>
                </x-card>
            </div>
        </div>
    </div>
    <x-confirm-modal title="Create User">
        <p>Are you sure?</p>
    </x-modal>
@endsection

@section('scripts')
    <script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>
    <script src="{{asset('assets/js/pages/crud/datatables/advanced/column-rendering.js')}}"></script>
@endsection

@section('styles')
    <link href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection
