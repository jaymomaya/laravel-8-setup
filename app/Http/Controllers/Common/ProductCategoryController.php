<?php

namespace App\Http\Controllers\Common;

use App\Common\ProductCategoryViewModel;
use App\DTO\Common\PostProductCategoryData as CommonPostProductCategoryData;
use App\Http\Controllers\Controller;
use App\Http\Requests\Common\CreateProductCategoryRequest;
use App\Models\Common\Comment;
use App\Models\Common\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PostProductCategoryData;

class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $viewModel = new ProductCategoryViewModel(new ProductCategory());
        return view('common.product_category.index', compact('viewModel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProductCategoryRequest $request)
    {
        DB::beginTransaction();
        $productCategory = ProductCategory::create($request->all());
        $comment = new Comment(['body' => $request->comment]);
        $productCategory->comments()->save($comment);
        DB::commit();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
