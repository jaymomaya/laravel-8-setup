<?php

namespace App\Enums\Common;

use Spatie\Enum\Laravel\Enum;

final class StateEnum extends Enum
{
    public static function values(): array
    {
        return [
            'ACTIVE' => 'Active',
            'INACTIVE' => 'Inactive',
            'TEMPORARY_INACTIVE' => 'Temporary Inactive',
            'PERMANENT_INACTIVE' => 'Permanent Inactive',
        ];
    }
}
