<?php

namespace App\DTO\Common;

use Illuminate\Http\Request;
use Spatie\DataTransferObject\DataTransferObject;

class PostProductCategoryData extends DataTransferObject
{
    public string $name;
    public string $state;
    public string $comment;

    public static function fromRequest(Request $request): self
    {
        return new self([
            'name' => $request->input('name'),
            'state' => $request->input('state'),
            'comment' => $request->input('comment'),
        ]);
    }
}
