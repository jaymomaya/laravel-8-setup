<?php

namespace App\Common;

use App\Enums\Common\StateEnum;
use App\Http\Controllers\Common\ProductCategoryController;
use App\Models\Common\ProductCategory;
use Spatie\ViewModels\ViewModel;

class ProductCategoryViewModel extends ViewModel
{
    public $indexUrl = null;

    public function __construct()
    {
        $this->state = StateEnum::values();
    }
}
