FROM php:8.0.2
RUN apt-get update -y && apt-get install -y openssl zip unzip git
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN apt-get install -y libonig-dev
RUN docker-php-ext-install pdo mbstring

WORKDIR /app
COPY . /app

CMD composer install
CMD php artisan cache:clear
CMD php artisan config:clear
CMD php artisan storage:link

CMD php artisan serve --host 0.0.0.0 --port=8181
EXPOSE 8181
