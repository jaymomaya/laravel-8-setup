<p align="center"><a href="https://www.targetpublications.org/" target="_blank"><img src="https://www.targetpublications.org/pub/media/logo/stores/1/logo-target-publications.png" width="400"></a></p>

## Requirements

- PHP 8.0.2
- Laravel 8.29
- PHPUnit 9.3.3

Clone the project and run: `composer update`

## Changes to be made
To make maatwebsite/excel plugin work properly with PHP8.0+ we need to edit our `php.ini` file and enable one extension `extension=gd`.
The above extension would be commented, so we need to remove that comment
