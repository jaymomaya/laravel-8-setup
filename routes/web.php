<?php

use App\Http\Controllers\Common\ProductCategoryController;
use App\Http\Controllers\Common\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
})->name('dashboard');

require __DIR__ . '/auth.php';

Route::get('test-components', function () {
    return view('test-components');
});

Route::resource('product-category', ProductCategoryController::class);
Route::resource('product', ProductController::class);
